<?php

namespace App\Service;

use Google\Client;
use Google_Service_Sheets;

class GoogleSheetClient
{
    /**
     * Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }
    /**
    * Returns an authorized API client.
    * @return Client the authorized client object
    */
   public function create()
   {
       $this->client->setApplicationName('Google Sheets API PHP Quickstart');
       $this->client->setScopes(Google_Service_Sheets::SPREADSHEETS_READONLY);
       $this->client->setAccessType('offline');
       $this->client->setPrompt('select_account consent');
   
       // Load previously authorized token from a file, if it exists.
       // The file token.json stores the user's access and refresh tokens, and is
       // created automatically when the authorization flow completes for the first
       // time.
       $tokenPath = 'token.json';
       if (file_exists($tokenPath)) {
           $accessToken = json_decode(file_get_contents($tokenPath), true);
           $this->client->setAccessToken($accessToken);
       }
   
       // If there is no previous token or it's expired.
       if ($this->client->isAccessTokenExpired()) {
           // Refresh the token if possible, else fetch a new one.
           if ($this->client->getRefreshToken()) {
               $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
           } else {
               // Request authorization from the user.
               $authUrl = $this->client->createAuthUrl();
               printf("Open the following link in your browser:\n%s\n", $authUrl);
               print 'Enter verification code: ';
               $authCode = trim(fgets(STDIN));
   
               // Exchange authorization code for an access token.
               $accessToken = $this->client->fetchAccessTokenWithAuthCode($authCode);
               $this->client->setAccessToken($accessToken);
   
               // Check to see if there was an error.
               if (array_key_exists('error', $accessToken)) {
                   throw new \Exception(join(', ', $accessToken));
               }
           }
           // Save the token to a file.
           if (!file_exists(dirname($tokenPath))) {
               mkdir(dirname($tokenPath), 0700, true);
           }
           file_put_contents($tokenPath, json_encode($this->client->getAccessToken()));
       }

       return $this->client;
   }
}
