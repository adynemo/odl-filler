<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ODLClient
{
    const CREATE_ARC_PATH = '/api/arc/create';

    /**
     * @var string
     */
    private $host;
    /**
     * @var string
     */
    private $token;
    /**
     * @var HttpClientInterface
     */
    private $client;

    public function __construct(string $ODLHost, string $ODLUserToken, HttpClientInterface $client)
    {
        $this->host = $ODLHost;
        $this->token = $ODLUserToken;
        $this->client = $client;
    }

    public function postArc(array $data): ResponseInterface
    {
        $response = $this->client->request(
            'POST',
            $this->host . self::CREATE_ARC_PATH,
            [
                'body' => $data,
                'headers' => [
                    'X-AUTH-TOKEN' => $this->token,
                ],
            ]
        );

        return $response;
    }
}
