<?php

namespace App\Command;

use App\Service\GoogleSheetClient;
use App\Service\ODLClient;
use Google_Service_Sheets;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class TransferArcCommand extends Command
{
    protected static $defaultName = 'odl:arc:transfer';

    /**
     * @var GoogleSheetClient
     */
    private $googleClient;
    /**
     * @var ODLClient
     */
    private $ODLClient;

    protected function configure()
    {
        $this->setDescription('Transfer arcs to ODL')
        ->setHelp('This command allows you to transfer arc from Google Sheet to ODL.');
    }

    public function __construct(GoogleSheetClient $googleClient, ODLClient $ODLClient, string $name = null)
    {
        parent::__construct($name);

        $this->googleClient = $googleClient;
        $this->ODLClient = $ODLClient;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        
        $era = $io->ask('Era Id');
        if (null === $era) {
            return self::FAILURE;
        }

        $spreadsheetId = $io->ask('Spread Sheet Id');
        if (null === $spreadsheetId) {
            return self::FAILURE;
        }

        $sheet = $io->ask('Sheet');
        if (null === $sheet) {
            return self::FAILURE;
        }
        
        $start = $io->ask('Start');
        if (null === $start) {
            return self::FAILURE;
        }
        
        $end = $io->ask('End');
        if (null === $end) {
            return self::FAILURE;
        }
        
        $client = $this->googleClient->create();
        $service = new Google_Service_Sheets($client);

        $range = sprintf('%s!%s:%s', $sheet, $start, $end);
        $response = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $response->getValues();

        if (empty($values)) {
            $io->info('No data found.');
        } else {
            foreach ($values as $row) {
                $io->info(implode(' || ', $row));

                $description = $row[2];
                if (isset($row[3])) {
                    $description .= ' ' . $row[3];
                }

                $response = $this->ODLClient->postArc([
                    'title' => $row[1],
                    'description' => $description,
                    'era' => $era,
                    'isEvent' => false,
                ]);

                if (201 === $response->getStatusCode()) {
                    $io->success('Created');
                }
            }
        }

        return self::SUCCESS;
    }
}
