# ODL Filler

A simple and basic command line tool to create arc from a Google Sheets to <a href='https://gitlab.com/adynemo/odl'>ODL</a>.

Requires an Google account and enable a <a href='https://developers.google.com/sheets/api/quickstart/php'>Google Sheets API</a>.

## Usage

Create `.env.local` from `.env` sample.

Fill the following variables:

- GOOGLE_API_KEY
- GOOGLE_CLIENT_ID
- GOOGLE_CLIENT_SECRET
- ODL_HOST
- ODL_USER_TOKEN

Then, run `bin/console odl:arc:transfer` and give:

- Era Id: id of the destination era for arcs creation.
- Spread Sheet Id: id of the spreadsheet provided by the url
- Sheet: the sheet title (on the tab)
- Start: the beginning column like `A` or cell `A2`
- End: the ending column like `D` or cell `D20`

The Google Sheets needs to be formatted like bellow:

| Position | Series   | Issues | Arc Title           |
| -------- | -------- | ------ | ------------------- |
| 1        | Watchmen | 1-12   | Watchmen            |
| 2        | Superman | 32-39  | The Men of Tomorrow |
