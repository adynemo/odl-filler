FROM ubuntu:latest
MAINTAINER Ady <ady@ecomail.io>

RUN useradd -ms /bin/bash odl

RUN apt update && \
    apt install -y software-properties-common && \
    add-apt-repository ppa:ondrej/php

RUN apt update && apt upgrade -y && \
    apt install -y wget composer make unzip nano curl sudo \
    php7.2 php7.2-cli php7.2-mbstring php7.2-intl \
    php7.2-gd php7.2-xml php7.2-zip \
    php7.2-curl php7.2-phpdbg

RUN sed -i -e "s/;date.timezone\s*=/date.timezone = Europe\/Paris/g" \
    -e "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" \
    -e "s/memory_limit = 128M/memory_limit=4096M/" \
    -e "s/;error_log = syslog/error_log = syslog/" \
    -e "s/upload_max_filesize\s*=\s*2M/upload_max_filesize = 5M/" \
    /etc/php/7.2/cli/php.ini

RUN wget https://get.symfony.com/cli/installer -O - | bash && \
    mv $HOME/.symfony/bin/symfony /usr/local/bin/symfony

RUN echo "odl  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/odl

USER odl

WORKDIR /app
